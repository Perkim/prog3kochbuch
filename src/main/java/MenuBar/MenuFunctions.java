package MenuBar;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import entities.Recipe;
import javafx.concurrent.Task;
import javafx.scene.control.Alert;
import javafx.stage.FileChooser;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.printing.PDFPageable;
import singleton.Cookbook;
import view.MainView;
import javax.print.*;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.standard.Copies;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.io.*;

/**
 * Klasse die Funktionen fuer die Menuleiste bereitstellt.
 * @author Iurie Golovencic, Vadim Khablov
 */
public class MenuFunctions {
    private static File lastDirectory = null;


    /**
     * Importieren einer Rezept-Datei
     * @return r Gibt das importierte Rezept zurueck
     */
    public static Recipe load(MainView mv) {
        Recipe r = null;
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open File");
        initFileChooser(fileChooser);
        File selectedFile = fileChooser.showOpenDialog(null);

        if (selectedFile != null) {
            try {
                InputStream targetStream = new FileInputStream(selectedFile);
                Reader reader = new InputStreamReader(targetStream, "UTF-8");
                Gson gson = new GsonBuilder().create();
                r = gson.fromJson(reader, Recipe.class);
                getLastFileDirectory(selectedFile);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            //TODO: Rezept der Datenbank hinzufügen
            Cookbook.getInstance().addRecipe(r);
            mv.showRecipe(r);
        }
        return r;
    }

    /**
     * Initialisiert Titel, Ordner-Pfad und FileTyp.
     * @param fileChooser der File Dialog
     *
     */
    public static void initFileChooser(FileChooser fileChooser){

        File defaultDirectory = new File(System.getProperty("user.home"));
        fileChooser.setInitialDirectory(lastDirectory != null ? lastDirectory : defaultDirectory);


        fileChooser.getExtensionFilters().addAll(//
                new FileChooser.ExtensionFilter("RCP-File", "*.rcp"));
    }


    /**
     * Speichert ein Rezept in einer .rcp Datei ab.
     * @return das exportierte File.
     */
    public static File save(Recipe recipe) {
        String recipeName = recipe.getName();
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Save File");
        initFileChooser(fileChooser);
        fileChooser.setInitialFileName(recipeName);
        File file = fileChooser.showSaveDialog(null);

        if (file != null) {
            try {
                saveJsonIntoFile(file, recipe);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } return file;
    }


    /**
     * Funktion zum drucken eines Rezepts.
     * (Da man unter Windoes nicht mit DocFlavor.INPUT_STREAM.PDF drucken kann (Exception) musste
     * Apache PDF-Box eingebunden werden um drucken zu können).
     */
    public static void sendToPrinter() {

        //Neuer Task
        Task task = new Task<Boolean>() {
            @Override
            public Boolean call() {

                //PDF Objekt instanzieren
                File PDFFile = new File("Recipe.pdf");

                try {
                    //Load PDF
                    PDDocument pd = PDDocument.load(PDFFile);
                    PrinterJob job = PrinterJob.getPrinterJob();
                    job.setPageable(new PDFPageable(pd));

                    //PrintDialog anzeigen
                    if (job.printDialog()) {
                        job.print();
                    }

                    pd.close();
                } catch (IOException | PrinterException ex) {
                }

                return true;
            }
        };
        //neuer Thread mit aktuellen Task
        new Thread(task).start();

    }

    /**
     * ################# Klappt nicht unter MAC ( freeze bei printDialog Aufruf)
     * Funktion zum drucken eines Rezepts.
     * (Da man unter Windoes nicht mit DocFlavor.INPUT_STREAM.PDF drucken kann (Exception) musste
     * Apache PDF-Box eingebunden werden um drucken zu können).
     *
     */
    public static void print() {
        PrintRequestAttributeSet pras = new HashPrintRequestAttributeSet();
        pras.add(new Copies(1));
        PrintService pss[] = PrintServiceLookup.lookupPrintServices(null, pras);
        PrintService defaultPrintService = PrintServiceLookup.lookupDefaultPrintService();
        PrintService selectedPrintService = ServiceUI.printDialog(null, 150, 150,
                pss, defaultPrintService, null, pras);
        if (pss.length == 0) {
            throw new RuntimeException("No printer services available.");
        }
        if(selectedPrintService != null) {
            PrintService myPrintService = selectedPrintService;
            System.out.println("Printing to " + myPrintService.getName());
            PDDocument document = null;
            try {
                document = PDDocument.load(new File("Recipe.pdf"));
            } catch (IOException e) {
                e.printStackTrace();
            }

            PrinterJob job = PrinterJob.getPrinterJob();
            job.setPageable(new PDFPageable(document));
            try {
                job.setPrintService(myPrintService);
            } catch (PrinterException e) {
                e.printStackTrace();
            }
            try {
                job.print();
            } catch (PrinterException e) {
                e.printStackTrace();
            }
        } else System.out.println("Druckvorgang abgebrochen.");
    }


    /**
     * Funktion die den Directory Ordner der zuletzt gespeicherten Datei return
     * @param lastFile zuletzt gespeicherte Datei
     * @return letzter verwendeter Ordner
     */
    public static File getLastFileDirectory(File lastFile) {
        if(lastFile != null) {
            lastDirectory = new File(lastFile.getParent());
        } return lastDirectory;
    }

    /**
     * Speichert das momentane Rezept in ein Json Object und schreibt es in das uebergebene File.
     * @param file das zu Speichernde File
     * @param r das zu konvertierende Objekt
     * @throws IOException
     */
    private static void saveJsonIntoFile(File file, Recipe r) throws IOException {
        boolean isCreated = file.createNewFile();
        if(isCreated){
            FileWriter writer = new FileWriter(file);
            Gson gson = new GsonBuilder().setPrettyPrinting().create();

            gson.toJson(r, writer);
            writer.close();
            new Alert(Alert.AlertType.INFORMATION, "Rezept wurde gespeichert als "+ file.getName()).showAndWait();
        }
    }
}
