package utility;
import java.io.*;

import com.pdfjet.*;
import javafx.stage.FileChooser;

/**
 * Hilfs-Klasse zum importieren eines Bilds.
 * @author Iurie Golovencic, Vadim Khablov
 */
public class CreateImage {

    /**
     * Erstellt ein Image-Objekt fuer die PDF
     * @param pdf uebergibt das Image dem PDF-Objekt
     * @param fileName Pfad zum Image
     * @return Eim Image-Objekt
     */
    public Image getImage(PDF pdf, String fileName) throws Exception {
        Image img;
        // Read from an input stream
        try (InputStream is = new BufferedInputStream(
                new FileInputStream(fileName))) {
            img = new Image(pdf, is, ImageType.JPG);
        }
        return img;
    }
    
    /**
     * Erstellt ein Image-Objekt zum benutzen in den Views
     * @param fileName Pfad zum Image
     * @return Ein Image-Objekt
     */
    public javafx.scene.image.Image getImage(String fileName) {
        File f = null;
        
        javafx.scene.image.Image image;
        
        try{
            f = new File(fileName);
            System.out.println(fileName);
            image = new javafx.scene.image.Image("file:" + fileName,150,150, false, false);
        }catch(NullPointerException e){
            System.out.println("does not exist");
            image = new javafx.scene.image.Image("file:hotdogs.jpg",150,150, false, false);
            
        }
 
        return image;
    }

    /**
     * Oeffnet einen FileChooser mit dem man einen Pfad zu einem Bild auswaelen kann.
     * @return Eine String path zu einem Bild
     */
    public String getPathFromFileChooser(){
        String path;

        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Bild Auswahl");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Image Files", "*.png", "*.jpg", "*.gif"));

        File selectedFile = fileChooser.showOpenDialog(null);

        if (selectedFile != null) {
            path = selectedFile.getAbsolutePath();
        }else{
            path = "Error";
        }

        System.out.println(path);

        return path;
    }
}
