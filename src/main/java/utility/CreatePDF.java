package utility;

import com.pdfjet.*;
import entities.Ingredient;
import entities.Recipe;
import entities.RecipeStep;
import java.io.FileOutputStream;
import java.util.*;

/**
 * Klasse zum ertellen einer Rezept-PDF.
 * @author Iurie Golovencic, Vadim Khablov
 */

public class CreatePDF {
    private static final float MARGIN_LEFT_BORDER = 60f;
    private static final float MARGIN_TOP_BORDER = 70f;
    private static float y3 = 0;
    private static String[] recipeStepArray = null;


    /**
     * Funktion zum erstellen einer Rezept PDF-Datei
     * @param recipe aktuelles Rezept
     * @throws Exception
     */
    public static void createPDF(Recipe recipe) throws Exception {
        // Init Data
        String recipeTitle = recipe.getName();
        String recipeDescription = recipe.getDescription();
        String cleanedRecipeDescription = recipeDescription.replaceAll("\n", "");
        List<Ingredient> recipeIngredients = recipe.getIngredients();
        List<RecipeStep> recipeSteps = recipe.getSteps();

        String[] recipeDesc = addLinebreaks(cleanedRecipeDescription, 50);
        //String[] test = recipeDesc.split("\\r?\\n");

        // Init File
        FileOutputStream fos = new FileOutputStream("Recipe.pdf");
        PDF pdf = new PDF(fos);
        Page page = new Page(pdf, Letter.PORTRAIT);

        // Init Font
        Font fontTimesBoldItalic = new Font(pdf, CoreFont.TIMES_BOLD_ITALIC);
        fontTimesBoldItalic.setSize(18);
        Font fontDesc = new Font(pdf, CoreFont.TIMES_ROMAN);
        fontDesc.setSize(12);
        Font fontTimesBold =  new Font(pdf, CoreFont.TIMES_BOLD);
        fontTimesBold.setSize(12);
        Font fontTimesRoman =  new Font(pdf, CoreFont.TIMES_ROMAN);
        fontTimesRoman.setSize(12);

        // Set Title
        TextLine textLine = new TextLine(fontTimesBoldItalic);
        textLine.setText(recipeTitle);
        textLine.setColor(Color.black);
        textLine.setLocation(MARGIN_LEFT_BORDER,MARGIN_TOP_BORDER);
        textLine.setTextEffect(Effect.SUPERSCRIPT);
        textLine.drawOn(page);

        // Set Description
        float y = 100f;
        for (String line : recipeDesc) {
            page.drawString(fontTimesRoman, line, MARGIN_LEFT_BORDER, y);
            y += fontTimesRoman.getBodyHeight();
        }

        // Init Image
        CreateImage image = new CreateImage();
        Image img = image.getImage(pdf, "hotdogs.jpg");

        // Set Image
        img.scaleBy(0.3);
        img.setPosition(350, MARGIN_TOP_BORDER);
        img.drawOn(page);

        // Set Ingredients
        TextLine textLine1 = new TextLine(fontTimesBold);
        textLine1.setText("Zutaten:");
        textLine1.setColor(Color.black);
        textLine1.setLocation(350,250);
        textLine1.setTextEffect(Effect.SUPERSCRIPT);
        textLine1.drawOn(page);

        int y1 = 260;
        for(int i = 0; i <recipeIngredients.size(); i++) {
            y1 = y1+15;
            TextLine textName = new TextLine(fontTimesRoman);
            textName.setText(recipeIngredients.get(i).getName());
            textName.setLocation(370,y1);
            textName.drawOn(page);
            TextLine textAmount = new TextLine(fontTimesRoman);
            textAmount.setText(String.valueOf(recipeIngredients.get(i).getPrice()));
            textAmount.setLocation(450, y1);
            textAmount.drawOn(page);
            TextLine textUnit = new TextLine(fontTimesRoman);
            textUnit.setText(recipeIngredients.get(i).getUnit());
            textUnit.setLocation(485, y1);
            textUnit.drawOn(page);
        }


        // Set Steps
        int lastStep = 0;
        if(recipeSteps.size() != 0) {
            lastStep = writeSteps(y, recipeSteps, fontTimesRoman, page, fontTimesBold, 0);
        }
        //Wenn Step über Seite geht
        if(y3 + (recipeStepArray.length*fontTimesRoman.getBodyHeight()) > 780f) {
            Page page2 = new Page(pdf , Letter.PORTRAIT);
            y = MARGIN_TOP_BORDER;
            writeSteps(y, recipeSteps, fontTimesRoman, page2, fontTimesBold, lastStep);
        }
        // PDF speichern
        pdf.flush();
        fos.close();

        }


    /**
     * Funktion um einen Zeilenumbruch nach einer bestimmten Satzlänge zu machen.
     * @param input der Satz
     * @param maxCharInLine max Satzlänge für newLine.
     * @return
     */
    private static String[] addLinebreaks(String input, int maxCharInLine) {
        StringTokenizer tok = new StringTokenizer(input, " ");
        StringBuilder output = new StringBuilder(input.length());
        int lineLen = 0;
        while (tok.hasMoreTokens()) {
            String word = tok.nextToken()+" ";

            while(word.length() > maxCharInLine){
                output.append(word.substring(0, maxCharInLine-lineLen) + "\n");
                word = word.substring(maxCharInLine-lineLen);
                lineLen = 0;
            }

            if (lineLen + word.length() > maxCharInLine) {
                output.append("\n");
                lineLen = 0;
            }
            output.append(word).append (" ");
            lineLen += word.length();
        }
        return output.toString().split("\n");
    }


    /**
     * Funktion um die Rezept-Schritte auf das PDF zu platzieren und zu schreiben.
     * @param y y-Höhe für ersten Schritt
     * @param recipeSteps Liste mit RezeptSchritten
     * @param stepCaptionFont
     * @param page PDF Seite aud die geschrieben wird
     * @param stepContentFont
     * @param firstStep erster Schritt für PDF Seite
     * @return letzter Schriitt der auf Seite passt
     * @throws Exception
     */
    public static int writeSteps(float y, List<RecipeStep> recipeSteps, Font stepCaptionFont,
                                  Page page, Font stepContentFont, int firstStep) throws Exception {
        float y2;
        int currentStep = 0;
        y2 = y +15;  // Höhe vom ersten Schritt
        for(int j = firstStep; j < recipeSteps.size(); j++) {
            y3 = y2 + stepContentFont.getBodyHeight(); // y-Höhe vor Step Zeile
            recipeStepArray = addLinebreaks(recipeSteps.get(j).getDescription(), 50);
            if (y3 + (recipeStepArray.length * stepContentFont.getBodyHeight()) <= 780f) { //Solange Steps auf Seite passen
                currentStep++;
                TextLine textLine3 = new TextLine(stepContentFont);
                textLine3.setText(j+1 + ". Schritt");
                textLine3.setLocation(MARGIN_LEFT_BORDER, y2);
                textLine3.setTextEffect(Effect.SUPERSCRIPT);
                textLine3.drawOn(page);
                y2 = y2 + (stepContentFont.getBodyHeight() * (recipeStepArray.length + 2));  // y-Höhe von i-ten Schritt
                for (String line : recipeStepArray) {
                    page.drawString(stepCaptionFont, line, MARGIN_LEFT_BORDER, y3);
                    y3 += stepCaptionFont.getBodyHeight();
                }
            }
        } return currentStep;
    }

}
