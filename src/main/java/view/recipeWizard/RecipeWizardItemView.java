package view.recipeWizard;

import utility.CreateImage;
import entities.Recipe;
import javafx.scene.layout.VBox;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;

/**
 * Eine Klasse zur Eingabe der basic Informationen eines Rezeptes,
 * wird als Teilstueck bei erstellung und edditierung von Rezepten benutzt.
 * 
 * @author Vladimir Naumov
 */
public class RecipeWizardItemView extends Pane{

    private static VBox outerVBox;
    private static HBox itemHBox;
    private static VBox infoVBox;
    private static VBox imageVBox;
    private static TextField nameTextField;
    private static TextArea descriptionTextArea;
    private static ImageView imageView;
    private static Button addImageButton;
    private static String path;
    private static TextField categoriesTextField;
    private static Boolean isNew;
    
    private static final String TF_NAME = "Name: ";
    private static final String TA_DESCRIPTION = "Beschreibung: ";
    private static final String TF_CATEGORIES = "Kategorien: ";
    private static final String BT_ADDIMAGE = "Add Bild";
            
    

    /**
     * Erstelung der NewRecipeItemView basierend an einer HBox.
     */
    public RecipeWizardItemView(Recipe recip) {
        try{
            path = recip.getPath();
        }catch(NullPointerException e){
            path = "Error";
        }
        
        infoVBox = new VBox();
        infoVBox.setSpacing(3);

        
        nameTextField = new TextField();
        try{
            nameTextField.setText(recip.getName());
        }catch(NullPointerException e){
            nameTextField.setPromptText(TF_NAME);
        }
        
        
        descriptionTextArea = new TextArea();
         try{
            descriptionTextArea.setText(recip.getDescription());
        }catch(NullPointerException e){
            descriptionTextArea.setPromptText(TA_DESCRIPTION);
        }
        
        descriptionTextArea.setWrapText(true);
        
        
        infoVBox.getChildren().addAll(nameTextField,descriptionTextArea);
        
        imageVBox = new VBox();
        imageVBox.setSpacing(3);
        
        imageView = new ImageView();
        imageView.setImage(new CreateImage().getImage(path));
        
        addImageButton = new Button();
        addImageButton.setText(BT_ADDIMAGE);
        addImageButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent value) {
                path = new CreateImage().getPathFromFileChooser();
                imageView.setImage(new CreateImage().getImage(path));
            }
        });
        
        imageVBox.getChildren().addAll(addImageButton,imageView);
        
        itemHBox = new HBox();
        itemHBox.setSpacing(3);
        itemHBox.getChildren().addAll(infoVBox,imageVBox);
        
        categoriesTextField = new TextField();
        categoriesTextField.setPromptText(TF_CATEGORIES);
                
        outerVBox = new VBox();
        outerVBox.getChildren().addAll(itemHBox,categoriesTextField);
        
        this.getChildren().addAll(outerVBox);
        
        outerVBox.setSpacing(3);
     
        outerVBox.prefWidthProperty().bind(this.widthProperty());
        categoriesTextField.prefWidthProperty().bind(outerVBox.widthProperty());
        itemHBox.prefWidthProperty().bind(outerVBox.widthProperty());
        infoVBox.prefWidthProperty().bind(itemHBox.widthProperty().subtract(imageVBox.widthProperty()));
        nameTextField.prefWidthProperty().bind(infoVBox.widthProperty());
        descriptionTextArea.prefWidthProperty().bind(infoVBox.widthProperty());
        descriptionTextArea.prefHeightProperty().bind(imageView.getImage().heightProperty());
        

    }
    

    public String getNameText() {
        return nameTextField.getText();
    }

    public String getDescriptionText() {
        return descriptionTextArea.getText();
    }

    public String getPath() {
        return path;
    }

    
    
}
