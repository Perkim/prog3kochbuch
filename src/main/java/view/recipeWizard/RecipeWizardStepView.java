
package view.recipeWizard;

import utility.CreateImage;
import entities.RecipeStep;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

/**
 *
 * @author VNaumov
 */
public class RecipeWizardStepView extends Pane{

    private static HBox infoHBox;
    private static VBox imageVBox;
//    private static HBox buttonsHBox;
    
    private static ImageView imageView;
    private static Button addImageButton;
    private static TextArea descriptionTextArea;
//    private static Button saveButton;
    
    private static SimpleStringProperty path;
    private static SimpleStringProperty description;
    private static RecipeStep step;
//    private static Boolean isNew;
//    private static BooleanProperty isChanged;
    
    private static final String TA_DESCRIPTION = "Beschreibung: ";
    private static final String BT_ADDIMAGE = "Add Bild";
//    private static final String BT_SAVE = "Speichern";
    
    public RecipeWizardStepView(RecipeStep st) {

//        if(st == null){
//            isNew = true;
//        }else{
//            isNew = false;
//        }
        step = st;
        
        description = new SimpleStringProperty();
        description.set(step.getDescription());
        
        path = new SimpleStringProperty();
        path.set(step.getPath());
 
        
//        isChanged = new SimpleBooleanProperty(false);
        
        descriptionTextArea = new TextArea();
        if(description == null){
            descriptionTextArea.setPromptText(TA_DESCRIPTION);
        }else{
            descriptionTextArea.setText(description.get());
        }
        
//        descriptionTextArea.setOnKeyTyped(new EventHandler<KeyEvent>() {
//            @Override
//            public void handle(KeyEvent value) {
//                isChanged.set(true);
//            }
//        });
        
        description.addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> listener, String oldValue, String newValue) {
                descriptionTextArea.setText(description.get());
            }
        });
        
        descriptionTextArea.setMinHeight(150);
        descriptionTextArea.setWrapText(true);

    
        
        imageVBox = new VBox();
        imageVBox.setSpacing(5);
        
        imageView = new ImageView();
        imageView.setImage(new CreateImage().getImage(path.get()));
        
        addImageButton = new Button();
        addImageButton.setText(BT_ADDIMAGE);
        addImageButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent value) {
                path.set(new CreateImage().getPathFromFileChooser());
                if(!path.get().equals(step.getPath())){
                    System.out.println(path);
                    System.out.println(st.getPath());
//                    isChanged.set(true);
                }
            }
        });
        path.addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> listener, String oldValue, String newValue) {
                imageView.setImage(new CreateImage().getImage(path.get()));
            }
        });
        addImageButton.setPrefWidth(imageView.getImage().widthProperty().subtract(5).divide(2).doubleValue());
        
//        isChanged.addListener( new ChangeListener<Boolean>() {
//                @Override
//                public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
//                    if (newValue) {
//                        saveButton.setDisable(false);
//                        System.out.println("Enabled");
//                    }else{
//                        saveButton.setDisable(true);
//                        System.out.println("Disabled");
//                    }
//                        
//                }
//            });
        
//        saveButton = new Button(BT_SAVE);
//        if(isNew == false){
//            saveButton.setDisable(true);
//        }
//        saveButton.setOnAction(new EventHandler<ActionEvent>() {
//            @Override
//            public void handle(ActionEvent value) {
//                save();
////                description.set(descriptionTextArea.getText());
////                step.setDescription(description.get());
////                step.setPath(path.get());
////                isChanged.set(false);
//            }
//        }); 
//        saveButton.setPrefWidth(imageView.getImage().widthProperty().subtract(5).divide(2).doubleValue());

//        
//        buttonsHBox = new HBox();
//        buttonsHBox.setPrefWidth(150);
//        buttonsHBox.getChildren().setAll(addImageButton,saveButton);
//        buttonsHBox.setSpacing(5);
        
//        imageVBox.getChildren().addAll(imageView,buttonsHBox);
        imageVBox.getChildren().addAll(imageView,addImageButton);
        imageVBox.setSpacing(5);
        imageVBox.prefWidthProperty().bind(imageView.getImage().widthProperty().add(6));
        
        infoHBox = new HBox();
        infoHBox.getChildren().addAll(descriptionTextArea,imageVBox);
        infoHBox.setSpacing(5);
        
        this.getChildren().addAll(infoHBox);
        
        infoHBox.prefHeightProperty().bind(this.heightProperty());
        infoHBox.prefWidthProperty().bind(this.widthProperty());
        descriptionTextArea.prefHeightProperty().bind(infoHBox.heightProperty());
        descriptionTextArea.prefWidthProperty().bind(infoHBox.widthProperty().subtract(imageVBox.widthProperty()));
        
        

        
    }
    
    private static void save(){
        description.set(descriptionTextArea.getText());
        step.setDescription(description.get());
        step.setPath(path.get());
//        isChanged.set(false);
        
    }

    public static SimpleStringProperty getPath() {
        return path;
    }

    public static SimpleStringProperty getDescription() {
        return description;
    }

    public static RecipeStep getStep() {
        save();
        return step;
    }

    public void setStep(RecipeStep step) {
        this.step = step;
        this.path.set(this.step.getPath());
        this.description.set(this.step.getDescription());
    }
    
    
    
    
}
