
package view.recipeWizard;

import entities.Ingredient;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 *
 * @author VNaumov
 */

public class RecipeWizardAddIngredientView {
    
    private static final String TITLE = "New Ingredient";
    private static final String TF_NAME = "Name: ";
    private static final String TF_AMOUNT = "Menge: ";
    private static final String TF_UNIT = "Einheit: ";
    private static final String BT_CREATE = "Erstellen";
    private static final String BT_CANCEL = "Abbrechen";
    private static final String LB_AMOUNT = "Die Menge wurde falsch eingegeben,\n bitte nur positeve Werte benutzten.";
    private static final String LB_ERROR = "Nicht alle Werte wurden eingegeben!";
    private static final String COLOR_RED = "ff0000";
    
    private static Stage stage;
    private static Scene scene;
    private static HBox ingredientHBox;
    private static HBox buttonsHBox;
    private static VBox outerVBox;
    private static Label messageLabel;
    private static TextField nameTextField;
    private static TextField amountTextField;
    private static TextField unitTextField;
    private static Button createButton;
    private static Button cancelButton;
    
    private static String name;
    private static double amount;
    private static String unit;
    private static Ingredient ingredient;
    
    public RecipeWizardAddIngredientView(){
        
        messageLabel = new Label();
        messageLabel.setTextFill(Color.web(COLOR_RED));
        
        nameTextField = new TextField();
        nameTextField.setPromptText(TF_NAME);
        
        amountTextField = new TextField();
        amountTextField.setPromptText(TF_AMOUNT);
        
        unitTextField = new TextField();
        unitTextField.setPromptText(TF_UNIT);
        
        ingredientHBox = new HBox();
        ingredientHBox.setSpacing(5);
        ingredientHBox.getChildren().addAll(nameTextField,amountTextField,unitTextField);
        
        createButton = new Button(BT_CREATE);
        createButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent value) {
                name = nameTextField.getText();
                
                try{
                    amount = Double.parseDouble(amountTextField.getText());
                }catch(NumberFormatException e){
                    messageLabel.setText(LB_AMOUNT);
                }
                    
                
                unit = unitTextField.getText();
                
                if((name != null && !name.isEmpty()) &&  amount > 0 &&
                         (unit != null && !unit.isEmpty())){
                    ingredient = new Ingredient(name,amount,unit);
                    stage.close();
                }else{
                    if(amount <= 0 ){
                        messageLabel.setText(LB_AMOUNT);
                    }else{
                        messageLabel.setText(LB_ERROR);
                    }
                }
                
            }
        });
        
        cancelButton = new Button(BT_CANCEL);
        cancelButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent value) {
                ingredient = null;
                stage.close();
            }
        });
        
        buttonsHBox = new HBox();
        buttonsHBox.getChildren().addAll(createButton,cancelButton);
        buttonsHBox.setSpacing(5);
        
        outerVBox = new VBox();
        outerVBox.setSpacing(5);
        outerVBox.setPadding(new Insets(3,3,3,3));
        outerVBox.getChildren().addAll(messageLabel,ingredientHBox,buttonsHBox);
        
        stage = new Stage();
        
        stage.initModality(Modality.APPLICATION_MODAL); 
            //prevents action with other windows
        stage.setTitle(TITLE);
        stage.setMinWidth(400);
        
        scene = new Scene(outerVBox);
        
        stage.setScene(scene);
        
        stage.showAndWait();
            // needs to be closed pevore going to an other stage
            
        messageLabel.prefWidthProperty().bind(outerVBox.widthProperty());
        nameTextField.prefWidthProperty().bind(ingredientHBox.widthProperty().divide(3));
        amountTextField.prefWidthProperty().bind(ingredientHBox.widthProperty().divide(3));
        unitTextField.prefWidthProperty().bind(ingredientHBox.widthProperty().divide(3));
        ingredientHBox.prefWidthProperty().bind(outerVBox.widthProperty());
        buttonsHBox.prefWidthProperty().bind(outerVBox.widthProperty());
        outerVBox.prefWidthProperty().bind(stage.widthProperty());
            
    }
    
    public Ingredient getIngredient(){
        return ingredient;
    }
}
