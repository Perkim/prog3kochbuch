/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view.recipeWizard;

import entities.Recipe;
import entities.RecipeStep;
import java.util.List;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;

/**
 *
 * @author VNaumov
 */
public class RecipeWizardStepControlView extends Pane{

    private static RecipeWizardStepView stepView;
    private static ObservableList<RecipeStep> steps;
    private static BorderPane outerBorderPane;
    private static HBox controlHBox;
    private static Button nextButton;
    private static Button prevButton;
    private static Button deleteButton;
    private static Button addButton;
    private static TextField pageTextField;
    private static int page;
    private static Boolean isDeleting = false;
    
    private static final String BT_NEXT = "->";
    private static final String BT_PREV = "<-";
    private static final String BT_ADD = "New Step";
    private static final String BT_DELETE = "Delete Step";
    
    public RecipeWizardStepControlView(Recipe recip) {
        
        steps = FXCollections.observableArrayList();
        try{
            steps.addAll(recip.getSteps());
        }catch(NullPointerException e){}
        page = 0;
        
        outerBorderPane = new BorderPane();
        
        pageTextField = new TextField();
        pageTextField.setText(Integer.toString(page+1));
        
        if(steps.isEmpty()){
            steps.add(new RecipeStep());
        }
        
        stepView = new RecipeWizardStepView(steps.get(page));
        outerBorderPane.setTop(stepView);
        
        
        pageTextField.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> listener, String oldValue, String newValue) {
                try{
                    int temp = Integer.parseInt(newValue)-1;
                    if(temp >= steps.size() || temp < 0){
                        pageTextField.setText(oldValue);
                    }else{
                        if(isDeleting == false){
                            steps.set(page, stepView.getStep());
                        }else{
                            isDeleting = false;
                        }
                        stepView.setStep(steps.get(temp));
                        page = temp;
                    }
                    
                }catch(NumberFormatException e){
                    if(!newValue.isEmpty()){
                        pageTextField.setText(oldValue);
                    }
                }
                
                
            }
        });
        
        nextButton = new Button(BT_NEXT);
        nextButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent value) {
                if(page+1 < steps.size()){
                    pageTextField.setText(Integer.toString(page+2));
                }
            }
        });
        
        prevButton = new Button(BT_PREV);
        prevButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent value) {
                if(page-1 >= 0){
                    pageTextField.setText(Integer.toString(page));
                }
            }
        });
        
        addButton = new Button(BT_ADD);
        addButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent value) {
                steps.add(new RecipeStep());
                pageTextField.setText(Integer.toString(steps.size()));
            }
        });
        
        deleteButton = new Button(BT_DELETE);
        deleteButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent value) {
                System.out.println(steps.get(page));
                System.out.println(steps.remove(page));
                isDeleting = true;
                pageTextField.setText(Integer.toString(page));
            }
        });
        
        
        controlHBox = new HBox();
        controlHBox.getChildren().addAll(deleteButton,prevButton,pageTextField,nextButton,addButton);
        outerBorderPane.setBottom(controlHBox);

        this.getChildren().addAll(outerBorderPane);
       
    }

    public List<RecipeStep> getSteps() {
        System.out.println(page);
        steps.set(page, stepView.getStep());
        stepView.getStep();
        System.out.println(steps.get(page));
        return steps;//.stream().collect(Collectors.toList());
    }
    
    
}
