package view.recipeWizard;

import entities.Recipe;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.layout.VBox;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import singleton.Cookbook;
import view.MainView;

/**
 * Eine Klasse zum Darstellen aller Rezepte die in Cookbook sind.
 * @author Vladimir Naumov
 */
public class RecipeWizardView extends Pane{

    private static VBox outerVBox;
    private static TabPane tabPane;
    private static Tab ingredientTab;
    private static Tab recipeStepTab;
    private static RecipeWizardIngredientView ingredientView;
    private static RecipeWizardItemView recipeItemView;
    private static Pane stepView;
    private static RecipeWizardStepControlView stepControlView;
    private static HBox controlHBox;
    private static Button cancelButton;
    private static Button acceptButton;
    
    private static Recipe recipe;
    private final MainView mainView;
    
    private static final String BT_ACCEPT = "OK";
    private static final String BT_CANCEL = "Abbrechen";

    /**
     * Erstelung der RecipeListViewE basierend an einer VBox.
     * @param main is the MainView of the stage and used to define the size
     */
    public RecipeWizardView(MainView main, Recipe r) {
        this.recipe = r;
        this.mainView = main;
        
        
        ingredientView = new RecipeWizardIngredientView(recipe);
        tabPane = new TabPane();
        
        recipeItemView = new RecipeWizardItemView(recipe);
        
        ingredientTab = new Tab("Ingredienten");
        ingredientTab.setContent(ingredientView);
        ingredientTab.setClosable(false);
        
        stepControlView = new RecipeWizardStepControlView(recipe);
        stepControlView.prefWidthProperty().bind(tabPane.widthProperty());
        stepControlView.prefHeightProperty().bind(tabPane.heightProperty());
        
        recipeStepTab = new Tab("Rezept Schritte");
        recipeStepTab.setContent(stepControlView);
        recipeStepTab.setClosable(false);
        
        tabPane.getTabs().addAll(ingredientTab,recipeStepTab);
        
        acceptButton = new Button(BT_ACCEPT);
        acceptButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent value) {
                recipe.setName(recipeItemView.getNameText());
                recipe.setDescription(recipeItemView.getDescriptionText());
                recipe.setPath(recipeItemView.getPath());
                recipe.setIngredients(ingredientView.getIngredientList());
                recipe.setSteps(stepControlView.getSteps());
                if(recipe.getId() == null){
                    Cookbook.getInstance().addRecipe(recipe);
                }else{
                    Cookbook.getInstance().update(recipe);
                }
                
                mainView.showRecipeList();
            }
        });
        acceptButton.setPrefWidth(70);
        
        cancelButton = new Button(BT_CANCEL);
        cancelButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent value) {
                mainView.showRecipeList();
            }
        });
        cancelButton.setPrefWidth(70);
      
        controlHBox = new HBox();
        controlHBox.getChildren().addAll(acceptButton,cancelButton);
        controlHBox.setSpacing(3);
        controlHBox.setAlignment(Pos.CENTER_RIGHT);
        
        outerVBox = new VBox();
        outerVBox.getChildren().addAll(recipeItemView,tabPane,controlHBox);;
        outerVBox.setPadding(new Insets(5,5,5,5));
        outerVBox.setSpacing(5);
        
        getChildren().addAll(outerVBox);

        this.prefWidthProperty().bind(mainView.widthProperty());
        this.prefHeightProperty().bind(mainView.heightProperty());
        outerVBox.prefWidthProperty().bind(this.widthProperty());
        outerVBox.prefHeightProperty().bind(this.heightProperty());
        
        recipeItemView.prefWidthProperty().bind(outerVBox.widthProperty());
        
        tabPane.prefHeightProperty().bind(outerVBox.heightProperty().subtract(250));
        tabPane.prefWidthProperty().bind(outerVBox.widthProperty());
        
        ingredientView.prefHeightProperty().bind(tabPane.heightProperty());
        ingredientView.prefWidthProperty().bind(tabPane.widthProperty());
        
        stepControlView.prefHeightProperty().bind(tabPane.heightProperty());
        stepControlView.prefWidthProperty().bind(tabPane.widthProperty());

    }
    
    public Recipe getRecipe(){
        if(recipe != null){
            return recipe;
        }else{
            return null;
        }
    }
}
