package view.recipeWizard;

import entities.Ingredient;
import entities.Recipe;
import java.util.List;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.util.Callback;

/**
 * Eine Klasse zur Verwaltung der Ingredienten von Rezepten,
 * wird in RecipeWizardView zur verwaltung von Rezepten benutzt.
 * @author VNaumov
 */
public class RecipeWizardIngredientView extends Pane{

    private static VBox outerVBox;
    private static TableView ingredientTableView;
    private static Button addButton;
    private static Button deleteButton;
    private static HBox buttonsHBox;
    private static Ingredient ingredient_temp;
    
    private static ObservableList<Ingredient> ingredientList;
    
    public RecipeWizardIngredientView(Recipe recip) {
        outerVBox = new VBox();
        outerVBox.setSpacing(3);
        
        ingredientList = FXCollections.observableArrayList();
        try{
            ingredientList.addAll(recip.getIngredients());
        }catch(NullPointerException e){}
       
        
        ingredientTableView = new TableView();
        
        TableColumn<Ingredient,String> nameCol = new TableColumn<>("Name");
        nameCol.setCellValueFactory(
                new PropertyValueFactory<>("name"));
        
        TableColumn<Ingredient, Double> amountCol = new TableColumn<>("Menge");
        amountCol.setCellValueFactory(new Callback<CellDataFeatures<Ingredient, Double>, ObservableValue<Double>>() {
            @Override
            public ObservableValue<Double> call(CellDataFeatures<Ingredient, Double> value) {
                return new ReadOnlyObjectWrapper(value.getValue().getPrice());
            }
        });
        
        
        TableColumn<Ingredient,String> unitCol = new TableColumn<>("Einheit");
        unitCol.setCellValueFactory(
                new PropertyValueFactory<>("unit"));
        
        ingredientTableView.setItems(ingredientList);
        ingredientTableView.getColumns().addAll(nameCol,amountCol,unitCol);
        
        
        addButton = new Button("+");
        addButton.setPrefWidth(50);
        addButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent value) {
                ingredient_temp = new RecipeWizardAddIngredientView().getIngredient();
                if(ingredient_temp != null){
                    System.out.println(ingredient_temp.toString());
                    ingredientList.add(ingredient_temp);
                    System.out.println(ingredientList.get(0).toString());
                }
            }
        });
        
        deleteButton = new Button("-");
        deleteButton.setPrefWidth(50);
        deleteButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent value) {
                ingredientList.remove(ingredientTableView.getSelectionModel().getSelectedItem());
            }
        });
        
        buttonsHBox = new HBox();
        buttonsHBox.getChildren().addAll(addButton,deleteButton);
        buttonsHBox.setAlignment(Pos.CENTER);
        buttonsHBox.spacingProperty().bind(this.widthProperty().divide(2).subtract(50));
        
        
        outerVBox.getChildren().addAll(ingredientTableView,buttonsHBox);
        
        getChildren().addAll(outerVBox);
        
        outerVBox.prefHeightProperty().bind(this.heightProperty());
        outerVBox.prefWidthProperty().bind(this.widthProperty());
        
        ingredientTableView.prefHeightProperty().bind(outerVBox.heightProperty().subtract(buttonsHBox.heightProperty()));
        ingredientTableView.prefWidthProperty().bind(outerVBox.widthProperty());

        nameCol.prefWidthProperty().bind(ingredientTableView.widthProperty().subtract(2).divide(3));
        amountCol.prefWidthProperty().bind(ingredientTableView.widthProperty().subtract(2).divide(3));
        unitCol.prefWidthProperty().bind(ingredientTableView.widthProperty().subtract(2).divide(3));
         
        buttonsHBox.prefWidthProperty().bind(this.widthProperty());

    }

    public List<Ingredient> getIngredientList() {
        return ingredientList;
    }
    
    
    
}
