package view.unused;

import entities.Ingredient;
import entities.Recipe;
import entities.RecipeStep;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.controlsfx.control.textfield.TextFields;
import singleton.Cookbook;
import view.MainView;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Robin Dort on 01.07.18.
 * Mit Scenebuilder gebaut - als prototyp gedacht. Wird nicht verwendet.
 */


public class SidebarController implements Initializable {

    private final MainView mainView;
    /**
     *  Attributes from the jfx Scene
     */
    @FXML
    private TextField recipeNameField;
    @FXML
    private TextField categoryField;
    @FXML
    private TextField descriptionField;
    @FXML
    private Button addRecipeButton;
    @FXML
    private TextArea listOfRecipesArea;
    @FXML
    private Button addStepButton;
    @FXML
    private Button pictureButton;
    @FXML
    private ImageView pictureView;

    private StepIngredientViewController stepIngredientViewController;
    private String recipePicturePath;


    private static final String DEFAULTPATH = "img/hotdogs.jpg";

    private static final String RECIPENAMEPATTERN = "[a-zA-Z]+[a-zA-Z]*";

    /**
     * Map to get the Ingredients from the StepIngredientViewController
     * List to get the Recipesteps from the StepIngredientViewController
     */
    List<Ingredient> ingredientList;
    List<RecipeStep> recipeList;

    public SidebarController(MainView view) {

        this.mainView = view;
    }


    /**
     * initialize method which gets called by opening the Scene
     * @param location
     * @param resource
     */
    public void initialize(URL location, ResourceBundle resource) {


        initAddStep();

        recipeNameField.setOnKeyReleased(this::enableButtonAddStep);
        categoryField.setOnKeyReleased(this::enableButtonAddStep);
        descriptionField.setOnKeyReleased(this::enableButtonAddStep);
        addStepButton.setOnAction(this::handleButtonAddStep);
        pictureButton.setOnAction(this::handleButtonAddPicture);
        String[] array = initializeArray();
        TextFields.bindAutoCompletion(categoryField,array);
        setListOfRecipesArea();
    }


    /**
     * method to handle the addStepButton
     * @param event events which gets triggered by clicking on the Button
     */
    private void handleButtonAddStep(ActionEvent event) {
        if (validateRecipeName()) {
            Stage actualStage = getActualStage(event);
            try {
                final URL fxmlUrl = getClass().getResource("/jfx/StepIngredientView.fxml");
                final FXMLLoader fxmlLoader = new FXMLLoader(fxmlUrl);
                fxmlLoader.setController(new StepIngredientViewController(mainView));
                final Parent root = fxmlLoader.load();
                stepIngredientViewController = fxmlLoader.getController();
                stepIngredientViewController.setCategory(getCategoryField());
                stepIngredientViewController.setRecipeName(getRecipeNameField());
                stepIngredientViewController.setDescription(getDescriptionField());
                if (getRecipePicturePath() != null) {
                    stepIngredientViewController.setPicturePath(getRecipePicturePath());
                } else {
                    stepIngredientViewController.setPicturePath(DEFAULTPATH);
                }
                mainView.setView(root);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void handleButtonAddPicture(ActionEvent event) {
        String filepath = chooseFile(event);
        File file = new File (filepath);
        Image image = new Image (file.toURI().toString());
        pictureView.setImage(image);
    }

    /**
     * method that allows users to choose an image from their fileStructure on the Desktop
     * @param event event which gets triggered by clicking on the addStepButton
     * @return the absolute path to the chosen file the user clicked on
     */
    private String chooseFile(ActionEvent event) {
        String s = "";
        Stage stage = (Stage) addStepButton.getScene().getWindow();


        FileChooser fileChooser = new FileChooser();
        //only allow files with .jpg and .png
        FileChooser.ExtensionFilter imageFilter = new FileChooser.ExtensionFilter("Image Files", "*.jpg", "*.png");
        fileChooser.getExtensionFilters().add(imageFilter);


        File file = fileChooser.showOpenDialog(stage);

        if (file != null) {
            s = file.getAbsolutePath();
        }
        recipePicturePath = s;
        return s;
    }

    /**
     * method to display the whole Recipe in the TextArea
     * @param ingredientList list including the ingredients of the Recipe
     * @param recipeList list including the Recipe Steps of the Recipe
     */
    public void displayContentInArea(String recipename, String description, String category, List<Ingredient> ingredientList, List<RecipeStep> recipeList) {

        listOfRecipesArea.appendText(recipeToString(recipename, description, category ,ingredientList, recipeList));

    }


    /**
     * method to set the edibility of the TextArea to false
     */
    private void setListOfRecipesArea() {
        listOfRecipesArea.setEditable(false);
    }


    /**
     * method to initialize an Array with predefined values
     * @return the filled Array
     */
    //TODO Sven -> Mehr vordefinierte Werte hier speichern -> idealerweise das Array mit allen bereits vorhandenen Rezeptnamen
    //TODO          der Datenbank füllen
    private String[] initializeArray() {
        // Hier Array mit bereits gefüllten Rezepten der DB füllen
        String array [] = {"Fleisch", "Suppe", "Nudeln", "Soße", "Salat"};
        ArrayList<Recipe> array2 = Cookbook.getInstance().getRecipeArrayList();
        return array;
    }


    /**
     * method to check if some Fields are still empty
     * @return true if some field is empty false otherwise
     */
    public boolean fieldsEmpty() {
        if (getRecipeNameField().isEmpty() || getCategoryField().isEmpty() || getDescriptionField().isEmpty()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * method to validate and match the RecipenameField to make sure a correct recipename is typed in
     * @return true if the recipename matches with the predefined regex -> false otherwise
     */
    private boolean validateRecipeName() {
        Pattern pattern = Pattern.compile(RECIPENAMEPATTERN);
        Matcher matcher = pattern.matcher(getRecipeNameField());

        if ( matcher.matches()) {
            return true;
        } else {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Falscher Rezeptname");
            alert.setHeaderText(null);
            alert.setContentText("Please insert a valid Recipename without Numbers like \"Bohnensuppe\"");

            alert.showAndWait();
            return false;
        }
    }


    /**
     * method to en- and disable the addRecipeButton
     * @param keyEvent event which gets triggered
     */
    private void enableButtonAddStep(KeyEvent keyEvent) {
        if (!fieldsEmpty()) {
            addStepButton.setDisable(false);
        } else {
            addStepButton.setDisable(true);
        }
    }

    /**
     * method to set the default disability of the addStepButton to true
     */
    private void initAddStep() {
        addStepButton.setDisable(true);
    }

    /**
     * get methods to get some attributes or the stage
     * @return the choosen attribute or the stage you´re in
     */

    public String getRecipeNameField() {
        return recipeNameField.getText();
    }

    public String getCategoryField() {
        return categoryField.getText();
    }

    public Stage getActualStage(ActionEvent event) {
        Stage actualStage = (Stage)  ((Node)event.getSource()).getScene().getWindow();
        return actualStage;
    }

    public String getDescriptionField() {
        return descriptionField.getText();
    }

    public String getRecipePicturePath() {
        return this.recipePicturePath;
    }

    public void setIngredientList(List<Ingredient> ingredientList) {
        this.ingredientList = ingredientList;
    }

    public void setRecipeList(List<RecipeStep> list ) {
        this.recipeList = list;
    }

    /**
     * toString method to display in the TextArea
     * @return the combined String
     */
    public String recipeToString(String name, String description, String category, List<Ingredient> ingredients, List<RecipeStep> list) {

        StringBuffer sb = new StringBuffer();
        int i = 1;
        sb.append("Rezept: ").append(name).append("\nDescription: ").append(description).append("\nCategorie: ").append(category).append("\n\n");
        sb.append("Ingredients:\n");
        for(Ingredient e : ingredients) {
            sb.append(e.toString()).append("\n");
        }
        sb.append("\n Schritte: \n");
        for(RecipeStep step : list) {
            sb.append("Schritt ").append(i).append(" ").append(step.getDescription().toString()).append("\n");
            i++;
        }

        return sb.toString();
    }






}
