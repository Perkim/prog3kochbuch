package view.unused;

import entities.Ingredient;
import entities.Recipe;
import entities.RecipeStep;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import singleton.Cookbook;
import view.MainView;
import java.io.File;
import java.net.URL;
import java.util.*;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Robin Dort on 10.07.18.*
 * Mit Scenebuilder gebaut - als prototyp gedacht. Wird nicht verwendet.
 */
public class StepIngredientViewController implements Initializable {

    private final MainView mainView;
    /**
     * Attributes from the jfx stage
     */

    @FXML
    private TextField amountField;
    @FXML
    private TextField ingredientField;
    @FXML
    private Button addIngredientButton;
    @FXML
    private TextField unitField;
    @FXML
    private TextField stepField;
    @FXML
    private TextArea ingredientArea;
    @FXML
    private TextArea stepArea;
    @FXML
    private Button addStepButton;
    @FXML
    private Button backButton;
    @FXML
    private Button addToRecipeButton;

    /**
     * Map to add ingredients
     * List to add RecipeSteps
     */
    private List<Ingredient> ingredientList;
    private List<RecipeStep> recipeStepList;

    private String recipeName;
    private String category;
    private String description;
    private String stepPath;
    private String picturePath;

    SidebarController sidebarController;

    private static final String AMOUNTPATTERN = "[1-9][1-9]*";
    private static final String UNITPATTERN = "[g|kg|stücke|l|ml]";
    private static final String INGREDIENTPATTERN = "[A-Z][a-z]*";
    private static final String DEFAULTPATH = "img/hotdogs.jpg";

    public StepIngredientViewController(MainView mainView) {
        this.mainView  = mainView;
    }

    /**
     * initilize method which gets called by setting the Scene
     * @param loacation
     * @param resource
     */
    public void initialize(URL loacation, ResourceBundle resource) {
        setIngredientList(ingredientList);
        setRecipeStepList(recipeStepList);

        amountField.setOnKeyReleased(this::enableButtonAddIngredient);
        ingredientField.setOnKeyReleased(this::enableButtonAddIngredient);
        unitField.setOnKeyReleased(this::enableButtonAddIngredient);
        addIngredientButton.setOnAction(this::handleButtonAddIngredient);
        addStepButton.setOnAction(this::handleButtonAddStep);
        stepField.setOnKeyReleased(this::enableButtonAddStep);
        backButton.setOnAction(this::handleBackButton);
        addToRecipeButton.setOnAction(this::handleAddToRecipeButton);
        initAddIngredient();
        initAddStep();
        initIngredientArea();
        initStepArea();
    }


    /**
     * method to validate and match the amountField to make sure a correct Amount was typed in
     * @return true if the typed amount matches with the predefined regex -> false otherwise
     */
    private boolean validateAmountField() {
        Pattern pattern = Pattern.compile(AMOUNTPATTERN);
        Matcher matcher = pattern.matcher(getAmountField());

        if (matcher.matches()) {
            return true;
        } else {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Falsche Mengenangabe");
            alert.setHeaderText(null);
            alert.setContentText("Please insert a valid Amount  like \"2\"");

            alert.showAndWait();
            return false;
        }
    }

    /**
     * method to validate and match the unitField to make sure a correct unit was typed in
     * @return true if the typed unit matches with the predefined regex -> false otherwise
     */
    private boolean validateUnitField() {
        Pattern pattern = Pattern.compile(UNITPATTERN);
        Matcher matcher = pattern.matcher(getUnitField());

        if (matcher.find()) {
            return true;
        } else {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Falsche Einheit");
            alert.setHeaderText(null);
            alert.setContentText("Please insert a valid Unit like \"g\" or \"kg\"");

            alert.showAndWait();
            return false;
        }
    }


    /**
     * method to validate and match the ingredientField to make sure a correct ingredient name  was typed in
     * @return true if the typed ingredient name matches with the predefined regex -> false otherwise
     */
    private boolean validateIngredientField() {
        Pattern pattern = Pattern.compile(INGREDIENTPATTERN);
        Matcher matcher = pattern.matcher(getIngredientField());

        if (matcher.find()) {
            return true;
        } else {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Falscher Name");
            alert.setHeaderText(null);
            alert.setContentText("Please insert a valid Ingredient name like \"Apfel\"");

            alert.showAndWait();
            return false;
        }
    }

    /**
     * method to handle the addIngredientButton
     * @param event event which gets triggered by clicking on the addIngredient Button
     */
    private void handleButtonAddIngredient(ActionEvent event) {
        if (validateAmountField() && validateUnitField() && validateIngredientField()) {
            String unit = getUnitField();
            Ingredient newIngredient = new Ingredient(getIngredientField(), Double.valueOf(getAmountField()), unit);

            ingredientList.add(newIngredient);
            displayContentInIngredientArea(ingredientList);
        }
    }

    /**
     * method to handle the addToRecipeButton
     * @param event event which gets triggered by clicking on the addToRecipeButton
     */
    private void handleAddToRecipeButton(ActionEvent event) {
        Stage stage = getActualStage(event);
        if (recipeName != null && category != null) {
            String recipename = getRecipeName();
            String category = getCategory();
            String description = getDescription();

            try {
                final URL fxmlUrl = getClass().getResource("/jfx/Sidebar.fxml");
                final FXMLLoader fxmlLoader = new FXMLLoader(fxmlUrl);
                fxmlLoader.setController(new SidebarController(mainView));
                final Parent root = fxmlLoader.load();
                sidebarController = fxmlLoader.getController();
                sidebarController.setIngredientList(ingredientList);
                sidebarController.setRecipeList(recipeStepList);
                sidebarController.displayContentInArea(recipename, description, category, getIngredientList(), getRecipeStepList());

                Recipe r = new Recipe();
                r.setName(getRecipeName());
                r.setPath(getPicturePath());
                r.setDescription(getDescription());
                r.setIngredients(getIngredientList());
                r.setSteps(getRecipeStepList());


                Cookbook.getInstance().addRecipe(r);

                mainView.setView(root);


            } catch (Exception e) {
                e.printStackTrace();
            }


        }
    }

    /**
     * method that allows users to choose an image from their fileStructure on the Desktop
     * @param event event which gets triggered by clicking on the addStepButton
     * @return the absolute path to the choosen file the user clicked on
     */
    private String chooseFile(ActionEvent event) {
        String s = "";
        Stage stage = (Stage) addStepButton.getScene().getWindow();


        FileChooser fileChooser = new FileChooser();
        //only allow files with .jpg and .png
        FileChooser.ExtensionFilter imageFilter = new FileChooser.ExtensionFilter("Image Files", "*.jpg", "*.png");
        fileChooser.getExtensionFilters().add(imageFilter);


        File file = fileChooser.showOpenDialog(stage);

        if (file != null) {
            s = file.getAbsolutePath();
        }
        stepPath = s;
        return s;
    }


    /**
     * method to set the default disability of the addStepButton to true
     * @param event event which gets triggered
     */
    private void enableButtonAddStep(KeyEvent event) {
        if (!stepFieldEmpty()) {
            addStepButton.setDisable(false);
        } else {
            addStepButton.setDisable(true);
        }
    }

    /**
     * method to handle the addStepButton
     * @param event event which gets triggered by clicking on the addStepButton
     */
    private void handleButtonAddStep(ActionEvent event) {


        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Image");
        alert.setHeaderText(null);
        alert.setContentText("Do you want to add an image to this step?");

        ButtonType yes = new ButtonType("YES");
        ButtonType no = new ButtonType("NO");

        alert.getButtonTypes().setAll(yes, no);


        Optional<ButtonType> result = alert.showAndWait();
        //User wants to add Image
        if (result.get() == yes) {

            String filename = chooseFile(event);
            RecipeStep recipeStep = new RecipeStep(filename, getStepField());
            recipeStepList.add(recipeStep);
            displayContentInStepArea();

            //User doesn´ t want to add image
        } else if (result.get() == no) {

            RecipeStep recipeStep = new RecipeStep(DEFAULTPATH, getStepField());
            recipeStepList.add(recipeStep);
            displayContentInStepArea();

        }
    }

    /**
     * method that allows the user go back to the Sidebar scene
     * @param event event which gets triggered by clicking on the backButton
     */
    private void handleBackButton(ActionEvent event) {
        Stage actualStage = getActualStage(event);
        setSidebarScene(actualStage);
    }


    /**
     * method to display the typed values of an ingredient in the ingredient TextArea
     * @param ingredient ArrayList which includes all of the ingredients the user has created
     */
    private void displayContentInIngredientArea(List<Ingredient> ingredient) {
        if (!ingredientFieldsEmpty()) {
            int i = 1;
            ingredientArea.setText(null);
            for (Ingredient e : ingredient) {
                ingredientArea.appendText("Zutat " + i + ": " + e.getName() + ", " + e.getPrice() +" " + e.getUnit() + "\n");
                i++;
            }
        }
    }

    /**
     * method to display the created steps to a recipe in the step TextArea
     */
    private void displayContentInStepArea() {
        if (!stepFieldEmpty()) {
            stepArea.setText(null);
            stepArea.appendText(stepToString());
        }
    }

    /**
     * method to set the default disability of the addIngredient button to true
     * @param keyEvent event which gets triggered
     */
    private void enableButtonAddIngredient(KeyEvent keyEvent) {
        if (!ingredientFieldsEmpty()) {
            addIngredientButton.setDisable(false);
        } else {
            addIngredientButton.setDisable(true);
        }
    }

    /**
     * method to check in some of the ingredient fields are still empty
     * @return true if some fields are empty -> false otherwise
     */
    private boolean ingredientFieldsEmpty() {
        if (getAmountField().isEmpty() || getUnitField().isEmpty() || getIngredientField().isEmpty()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * method to check if the stepField is still empty
     * @return true if the field is empty -> false otherwise
     */
    private boolean stepFieldEmpty() {
        if (getStepField().isEmpty()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * method to set the default disability of the addIngredient button to true
     */
    private void initAddIngredient() {
        if (ingredientFieldsEmpty()) {
            addIngredientButton.setDisable(true);
        } else {
            addIngredientButton.setDisable(false);
        }
    }


    /**
     * method to set the default disability of the addStep button to true
     */
    private void initAddStep() {
        if (stepFieldEmpty()) {
            addStepButton.setDisable(true);
        } else {
            addStepButton.setDisable(false);
        }
    }

    /**
     * method to set the edibility of the ingredient TextArea to false
     */
    private void initIngredientArea() {
        ingredientArea.setEditable(false);
    }

    /**
     * method to set the edibility of the step TextArea to false
     */
    private void initStepArea() {
        stepArea.setEditable(false);
    }


    /**
     * get methods to get some attributes / the list / the map or the actual stage you´re in
     * @return the choosen attribute / the list / the map or the actual stage
     */
    public String getAmountField() {
        return amountField.getText();
    }

    public String getIngredientField() {
        return ingredientField.getText();
    }

    public String getUnitField() {
        return unitField.getText();
    }

    public String getStepField() {
        return stepField.getText();
    }

    public List<RecipeStep> getRecipeStepList() {
        return recipeStepList;
    }

    public List<Ingredient> getIngredientList() {
        return ingredientList;
    }

    public Stage getActualStage(ActionEvent event) {
        Stage actualStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        return actualStage;
    }

    public void setRecipeName(String name) {
        this.recipeName = name;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setPicturePath(String picturePath) {
        this.picturePath = picturePath;
    }

    private void setIngredientList(List<Ingredient> ingredientList) {
        this.ingredientList = new ArrayList<>();
    }

    private void setRecipeStepList(List<RecipeStep> recipeStepList) {
        this.recipeStepList = new ArrayList<>();
    }

    public String getPicturePath() {
        return this.picturePath;
    }

    public String getRecipeName() {
        return this.recipeName;
    }

    public String getCategory() {
        return this.category;
    }

    public String getDescription() {
        return this.description;
    }

    public String getStepPath() {
        return this.stepPath;
    }



    /**
     * method to set the sidebar view again
     * @param primaryStage the actual stage you´re in
     */
    private void setSidebarScene(Stage primaryStage) {
        try {
            final URL fxmlUrl = getClass().getResource("/jfx/Sidebar.fxml");
            final FXMLLoader fxmlLoader = new FXMLLoader(fxmlUrl);
            fxmlLoader.setController(new SidebarController(mainView));
            final Parent root = fxmlLoader.load();
            mainView.setView(root);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * toString method to display in the TextArea
     * @return the combined String
     */
    public String stepToString() {
        StringBuffer sb = new StringBuffer();

        for (RecipeStep recipeStep : recipeStepList) {
            sb.append("Schritt ").append(recipeStepList.indexOf(recipeStep) + 1).append(": ").append(recipeStep.getDescription()).append("\n");
        }

        return sb.toString();
    }
}