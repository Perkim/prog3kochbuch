package view;

import MenuBar.*;
import entities.Recipe;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import utility.CreatePDF;

import java.io.*;


/**
 *
 * @author VNaumov,IGolovencic,VKhablov
 */
public class MenuBarView extends MenuBar{
    private final static Menu menuDatei = new Menu("Datei");
    private final static MenuItem menuItemToList = new MenuItem("Recept Liste");
    private final static MenuItem menuItemNeu = new MenuItem("Neu");
    private final static MenuItem menuItemOeffnen = new MenuItem("Oeffnen");
    private final static MenuItem menuItemSpeichern = new MenuItem("Speichern");
    private final static MenuItem menuItemDrucken = new MenuItem("Drucken");
    private final static MenuItem menuItemBearbeiten = new MenuItem("Bearbeiten");

    public MenuBarView(final MainView mainView) {
        this.getMenus().addAll(menuDatei);
        menuDatei.getItems().addAll(menuItemNeu,menuItemBearbeiten,menuItemOeffnen,menuItemSpeichern,menuItemToList, menuItemDrucken);
        
        menuItemToList.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent value) {
                mainView.showRecipeList();
                System.out.println("I should show RecipeList now");
            }
        });
        
        menuItemNeu.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent value) {
                mainView.showRecipeWizard(new Recipe());
                System.out.println("I should show RecipeWizardView now");
            }
        });
        
        menuItemBearbeiten.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent value) {
                mainView.showRecipeWizard(mainView.getSelectedRecipe());
                System.out.println("I should show RecipeWizardView now");
            }
        });
        
        menuItemOeffnen.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent value) {
                MenuFunctions.load(mainView);

            }
        });

        menuItemSpeichern.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent value) {
                File savedFile = MenuFunctions.save(mainView.getSelectedRecipe());
                MenuFunctions.getLastFileDirectory(savedFile);
            }
        });

        menuItemDrucken.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent value) {


                try {
                    CreatePDF.createPDF(mainView.getSelectedRecipe());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                MenuFunctions.sendToPrinter();

            }
        });
    }

    /**
     * Blendet die jeweiligen Menueintraege in der MenuBar aus, wenn diese nicht gebraucht werden.
     * @param b ein oder ausblenden
     */
    public void toggleSaveAndPrint(boolean b) {
        menuItemSpeichern.setDisable(b);
        menuItemDrucken.setDisable(b);
    }

}
