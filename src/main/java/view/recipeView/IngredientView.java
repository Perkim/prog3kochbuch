package view.recipeView;

import entities.Ingredient;
import javafx.beans.binding.Bindings;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import java.util.List;

/**
 * View Class that displays ingredients of selected recipe in IngredientView.
 *
 * @author Kerim Dennis Weisenseel
 * @version 19.08.2018
 */
public class IngredientView extends VBox {

    private TableView table;
    private HBox editBar;
    private Button decrease;
    private Button increase;
    private TextField amount;
    private int amountFactor;
    private List<Ingredient> ingredients;
    public ObservableList<IngredientTableViewClass> data = FXCollections.observableArrayList();

    /**
     * Displays contents of selected recipe and implements Table View.
     *
     * @param ingredients ingredient List of selected recipe
     */
    public IngredientView(List<Ingredient> ingredients) {
        this.ingredients = ingredients;
        editBar = new HBox();
        table = new TableView();

        decrease = new Button("-");
        increase = new Button("+");
        amount = new TextField();
        amountFactor = 1;

        decrease.setOnAction(new EventHandler<ActionEvent>() {
            /**
             * Decreases factor by which the ingredientamount is multiplied by one.
             *
             * @param event ActionEvent
             */
            @Override
            public void handle(ActionEvent event) {
                setAmountFactor(getAmountFactor()-1);
                amount.setText(Integer.toString(amountFactor));
            }
        });

        increase.setOnAction(new EventHandler<ActionEvent>() {
            /**
             * Increases factor by which the ingredientamount is multiplied by one.
             *
             * @param event ActionEvent
             */
            @Override
            public void handle(ActionEvent event) {
                setAmountFactor(getAmountFactor()+1);
                amount.setText(Integer.toString(amountFactor));
            }
        });

        TableColumn nameColumn = new TableColumn("Name");
        nameColumn.setCellValueFactory(new PropertyValueFactory<IngredientTableViewClass, String>("nameTableViewClass"));
        TableColumn amountColumn = new TableColumn("Amount");
        amountColumn.setCellValueFactory(new PropertyValueFactory<IngredientTableViewClass, String>("amountTableViewClass"));
        TableColumn unitColumn = new TableColumn("Unit");
        unitColumn.setCellValueFactory(new PropertyValueFactory<IngredientTableViewClass, String>("unitTableViewClass"));

        table.setItems(data);
        table.getColumns().addAll(nameColumn, amountColumn, unitColumn);

        table.setFixedCellSize(25);
        table.prefHeightProperty().bind(table.fixedCellSizeProperty().multiply(Bindings.size(table.getItems()).add(1.40)));
        table.minHeightProperty().bind(table.prefHeightProperty());
        table.maxHeightProperty().bind(table.prefHeightProperty());

        amount.setText(Integer.toString(amountFactor));

        editBar.getChildren().addAll(decrease, amount, increase);
        getChildren().addAll(table, editBar);


        updateIngredients(ingredients, amountFactor);
    }

    /**
     * Updates Table View with new amount factor by calling updateIngredients.
     * Also checks for amountFactor being greater than 0.
     *
     * @param amountFactor factor by which ingredient amount is multiplied
     */
    public void setAmountFactor(int amountFactor) {
        if(amountFactor > 0) {
            this.amountFactor = amountFactor;
            updateIngredients(ingredients, amountFactor);
        }
    }

    /**
     * Gets amount factor.
     *
     * @return the factor by which ingredient amount is multiplied
     */
    public int getAmountFactor() {
        return amountFactor;
    }

    /**
     * Fills Table View with ingredients List and multiplies ingredient amount with amount factor.
     *
     * @param ingredients ingredients List of selected recipe
     * @param factor factor by which ingredient amount is multiplied
     */
    private void updateIngredients(List<Ingredient> ingredients, int factor){
        data.clear();

        for(Ingredient i : ingredients) {
            data.add(new IngredientTableViewClass(i.getUnit(),i.getName(),i.getPrice() * factor));
        }
    }


    /**
     * Supplies Data for Table View.
     */
    public static class IngredientTableViewClass {
        private String unitTableViewClass = "";
        private String nameTableViewClass = "";
        private double amountTableViewClass = 0.0;

        /**
         * Sets values of Class Variables to values delivered to constructor.
         *
         * @param unitTableViewClassX unit attribute used by Table View
         * @param nameTableViewClassX name attribute used by Table View
         * @param amountTableViewClassX amount attribute used by Table View
         */
        private IngredientTableViewClass(String unitTableViewClassX, String nameTableViewClassX, double amountTableViewClassX) {
            unitTableViewClass = unitTableViewClassX;
            nameTableViewClass = nameTableViewClassX;
            amountTableViewClass = amountTableViewClassX;
        }

        /**
         * Getter.
         *
         * @return unitTableViewClass
         */
        public String getUnitTableViewClass() {
            return unitTableViewClass;
        }

        /**
         * Setter.
         *
         * @param unitTableViewClass unitTableViewClass
         */
        public void setUnitTableViewClass(String unitTableViewClass) {
            this.unitTableViewClass = unitTableViewClass;
        }

        /**
         * Getter.
         *
         * @return nameTableViewClass
         */
        public String getNameTableViewClass() {
            return nameTableViewClass;
        }

        /**
         * Setter.
         *
         * @param nameTableViewClass nameTableViewClass
         */
        public void setNameTableViewClass(String nameTableViewClass) {
            this.nameTableViewClass = nameTableViewClass;
        }

        /**
         * Getter.
         *
         * @return amountTableViewClass
         */
        public double getAmountTableViewClass() {
            return amountTableViewClass;
        }

        /**
         * Setter.
         *
         * @param amountTableViewClass amountTableViewClass
         */
        public void setAmountTableViewClass(double amountTableViewClass) {
            this.amountTableViewClass = amountTableViewClass;
        }
    }
}
