package view.recipeView;

import entities.Recipe;
import javafx.scene.layout.VBox;
import view.MainView;

/**
 * Main View class for RecipeView that holds all its other Views.
 *
 * @author kerim Dennis Weisenseel
 * @version 19.08.2018
 */
public class RecipeView extends VBox {

    private MainView mainView;
    private StepView stepView;
    private RecipeDataView recipeDataView;
    private Recipe recipe;

    /**
     * Displays RecipeView with contents of loaded recipe.
     *
     * @param view Main View
     * @param r the selected recipe
     */
    public  RecipeView(MainView view, Recipe r) {
        this.mainView = view;
        this.recipe = r;
        System.out.println("I should be showing " + r + "now.");
        stepView = new StepView(r.getSteps());
        recipeDataView = new RecipeDataView(r);

        getChildren().addAll(recipeDataView, stepView);
    }

}
