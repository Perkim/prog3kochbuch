package view.recipeView;

import entities.RecipeStep;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.text.Font;

import java.util.List;

/**
 * View class for showing the step description of this recipe.
 *
 * @author kerim Dennis Weisenseel
 * @version 19.08.2018
 */
public class StepView extends BorderPane {

    private Label content;
    private Label counterLabel;
    private Button slideLeftButton;
    private Button slideRightButton;

    private List<RecipeStep> steps;

    int counter = 1;

    /**
     * Displays contents of the selected recipe.
     *
     * @param steps steps text of selected recipe
     */
    public StepView(List<RecipeStep> steps) {

        this.steps = steps;
        content = new Label("Empty");
        counterLabel = new Label("0");

        counterLabel.setFont(new Font(20.0));
        counterLabel.setStyle("-fx-font-weight: bold");


        slideLeftButton = new Button("<--");

        slideLeftButton.setOnAction(new EventHandler<ActionEvent>() {
            /**
             * Handles event by calling slideLeft method.
             *
             * @param event Actionevent
             */
            public void handle(ActionEvent event) {
                slideLeft();
            }
        });


        slideRightButton = new Button("-->");

        slideRightButton.setOnAction(new EventHandler<ActionEvent>() {
            /**
             * Handles event by calling slideRight method.
             *
             * @param event Actionevent
             */
            public void handle(ActionEvent event) {
                slideRight();
            }
        });

        if(!steps.isEmpty()) {
            content.setText(steps.get(counter-1).getDescription());
            setCounterLabel(counter);
        }

        setCenter(content);
        setLeft(slideLeftButton);
        setRight(slideRightButton);
        setTop(counterLabel);
        setAlignment(counterLabel, Pos.CENTER);

    }

    /**
     * Sets counterLabel value to value of selected step.
     *
     * @param counter value to set the counter label to
     */
    private void setCounterLabel(int counter) {
        counterLabel.setText("Step: " + counter);
    }

    /**
     * Sets Step description to content of selected step.
     *
     * @param counter number of step that needs to be displayed
     */
    private void setDescription(int counter) {
        content.setText(steps.get(counter-1).getDescription());
    }

    /**
     * Sets Step image to content of selected step.
     *
     * @param counter number of stepimage that needs to be displayed
     */
    private void setImage(int counter) {
        RecipeDataView.setImage(steps.get(counter-1).getPath());
    }

    /**
     * Decreases counter by one and displays updated counter label by calling setCounterLabel.
     */
    public void slideLeft() {
//        counter--;
//        if(counter < 1 ){
//            counter = 1;
//        }
//        content.setText(steps.get(counter + 1).getDescription());
//        setCounterLabel(counter);
//        setDescription(counter);
//        setImage(counter);
        System.out.println(counter);
        if(counter-1 > 0){
            counter--;
            //content.setText(steps.get(counter - 1).getDescription());
            setCounterLabel(counter);
            setDescription(counter);
            setImage(counter);
        }
        content.setText(steps.get(counter - 1).getDescription());
        setCounterLabel(counter);
        setDescription(counter);
        setImage(counter);

    }

    /**
     * Increases counter by one and displays updated counter label by calling setCounterLabel.
     */
    public void slideRight() {
//        counter++;
//        if(counter > steps.size() ){
//            counter = steps.size();
//        }
//        setCounterLabel(counter);
//        setDescription(counter);
//        setImage(counter);

        System.out.println(counter);
        System.out.println(steps.size());
        if(counter+1 <= steps.size()){
            counter++;
            //content.setText(steps.get(counter - 1).getDescription());
            setCounterLabel(counter);
            setDescription(counter);
            setImage(counter);
        }
    }

}
