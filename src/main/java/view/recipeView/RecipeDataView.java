package view.recipeView;

import utility.CreateImage;
import entities.Recipe;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;


/**
 * View that is part of the RecipeView, ontains the IngredientView and the recipe image.
 *
 * @author kerim Dennis Weisenseel
 * @version 19.08.2018
 */
public class RecipeDataView extends HBox {

    private ImageView imageView;
    private static Image image;
    private IngredientView actualIngredientView;

    /**
     * Displays contents of selected recipe.
     *
     * @param r selected recipe
     */
    public RecipeDataView(Recipe r) {

//        File f = new File(r.getPath());
//        if(!f.exists()) {
//            System.out.println("does not exist");
//            image = new Image("file:hotdogs.jpg",150,150, false, false);
//        } else {
//            System.out.println(r.getPath());
//            image = new Image("file:" + r.getPath(),150,150, false, false);
//        }
        image = new CreateImage().getImage(r.getPath());

        imageView = new ImageView(image);
        actualIngredientView = new IngredientView(r.getIngredients());

        imageView.setPreserveRatio(true);
        imageView.setFitHeight(200);

        getChildren().addAll(imageView, actualIngredientView);

    }

    /**
     * sets image of Recipe data view to string specified in argument.
     *
     * @param path path of image file to set image view to
     */
    public static void setImage(String path){
        image = new CreateImage().getImage(path);
    }
}