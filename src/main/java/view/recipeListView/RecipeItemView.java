package view.recipeListView;

import utility.CreateImage;
import entities.Recipe;
import javafx.geometry.Insets;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;


/**
* 
* RecipeItemView ist eine HBox zur darstellung von Recepten mit einem Bild, 
* dem Namen und der Beschreibung dieses.
* @author Vladimir Naumov
* 
*/

public class RecipeItemView extends HBox {

    private final Recipe recipe;


    /**
    * Erstellung des WebView in einem ScrollPane
    * @param name Name of Recipe
    * @param description Description of Recipe
    * @return Returns A ScrollPane with informations of the Recipe
    */
    private ScrollPane createDispley(String name, String description) {
        /*
        * Ein WebView wird benutzt zur automatischen anpassung der Umbr�che 
        * auf dem vorgegebenem Platz
        */
        WebView wv = new WebView();
        wv.setContextMenuEnabled(false);
        WebEngine we = wv.getEngine();
        we.loadContent("Name: "+ name + "<div>Beschreibung: " + description + "</div>");

        /*
        * ScrollPane wird benutzt zur kompremierung des Platzes
        */
        ScrollPane scroll = new ScrollPane();
        scroll.setContent(wv);
        scroll.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        scroll.setVbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);
        scroll.setPadding(new Insets(1,1,1,1));
        scroll.prefWidthProperty().bind(this.widthProperty().subtract(0));
        scroll.prefViewportWidthProperty().bind(this.widthProperty().subtract(12));
        scroll.prefViewportHeightProperty().bind(this.heightProperty().subtract(2));


        wv.prefWidthProperty().bind(scroll.widthProperty());
        wv.prefHeightProperty().bind(scroll.prefViewportHeightProperty());
        scroll.prefViewportWidthProperty().bind(this.widthProperty());
        scroll.prefHeightProperty().bind(this.heightProperty());

        return scroll;
    }

    /**
    * Erstellung der RecipeItemView, diese Gibt ein Bild und die Beschreibung 
    * eines Rezeptes an.
    * @param recipe the Recipe on with the HBox is based on.
    */
    public RecipeItemView(Recipe recipe) {

        ImageView iv = new ImageView();
        iv.setImage(new CreateImage().getImage(recipe.getPath()));
        
        ScrollPane scroll = createDispley(recipe.getName(), recipe.getDescription());

        setPrefHeight(150);
        getChildren().addAll(iv, scroll);
        this.recipe = recipe;
    }

    /**
    * Ein Geter f�r die r�ckgabe der Recipe
    * @return Returns the recipe of the RecipeItemView
    */
    public Recipe getRecipe() {
        return recipe;
    }
}
