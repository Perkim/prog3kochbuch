package view.recipeListView;

import javafx.application.Platform;


/**
 * Eine Klasse zum regeln der Mausklicke in RecipeListViewE
 * @author Vladimir Naumov
 */
public class ClickRunner implements Runnable{
    // Die Zeit, die nach einem Klick gewartet wird um zu entscheiden,
    // ob es sich um einen singel oder doppel Klick handelt.
    // in ms
    private static final int SINGLE_CLICK_DELAY = 250;
    
    // Was bei Singelklick gemacht werden soll.
    private final Runnable onSingleClick;
    
    // ob Singelklick abgebrochen werden soll 
    // und Doppelklick durchgefuehrt werden soll.
    private boolean aborted = false;

    /**
     * Festlegung was bei Singelklick gemacht werden soll
     * @param onSingleClick Die Runnable fuer Singelklick
     */
    public ClickRunner(Runnable onSingleClick) {
        this.onSingleClick = onSingleClick;
    }
    
    /**
     * Abbruch des Singelklicks.
     */
    public void abort(){
        this.aborted = true;
    }
    
    /**
     * Ueberpruefung ob es sich um singel oder doppel Klick handelt.
     */
    @Override
    public void run() {
        try {
            Thread.sleep(SINGLE_CLICK_DELAY);
        }catch (InterruptedException e){
            e.printStackTrace();
        }
        if (!aborted){
            Platform.runLater(() -> onSingleClick.run());
        }
    }
}
