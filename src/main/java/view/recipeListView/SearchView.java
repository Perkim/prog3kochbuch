package view.recipeListView;

import java.util.List;
import java.util.function.Consumer;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.ComboBox;

/**
 * Die Klasse f�r die Suchfunktion, die in RecipeListViewE benutzt wird. 
 * @author Vladimir Naumov
 * 
 */
public class SearchView extends ComboBox<String>{

    /**
     * Erstellt die SearchView in Form von ComboBox 
     * @param recipeItemViewList  is the List on which the search should work.
     */
    public SearchView(List<RecipeItemView> recipeItemViewList) {
        // Einstellungen der CompoBox
        // Anfang
        setEditable(true);
        setPromptText("Filter: ");
        // Einstellungen der CompoBox
        // Ende
        
        // Erstellung der Vorgaben, die in der Suchleiste angezeigt werden
        // Anfang
        ObservableList<String> RecipeNames = FXCollections.observableArrayList();
        recipeItemViewList.forEach(new Consumer<RecipeItemView>() {
            @Override
            public void accept(RecipeItemView action) {
                RecipeNames.add(action.getRecipe().getName());
            }
        });
        this.setItems(RecipeNames);
        // Erstellung der Vorgaben,
        // Ende
        
        
        // Die Suchfunktion. 
        // Haengt von dem Namen und der Beschreibung von Rezepten ab.
        // Anfang
        setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                String filter;
                filter = getValue();
                if(!filter.equals("") && filter != null){
                    for(int i = 0; i < recipeItemViewList.size();i++){
                        
                        //recipeItemViewList.get(i).getRecipe().;
                        if(recipeItemViewList.get(i).getRecipe().getName().toLowerCase().contains(filter.toLowerCase())
                                || recipeItemViewList.get(i).getRecipe().getDescription().toLowerCase().contains(filter.toLowerCase())){
                            
                            recipeItemViewList.get(i).setVisible(true);
                            recipeItemViewList.get(i).managedProperty().bind(recipeItemViewList.get(i).visibleProperty());
                        }
                        else{
                            recipeItemViewList.get(i).setVisible(false);
                            recipeItemViewList.get(i).managedProperty().bind(recipeItemViewList.get(i).visibleProperty());
                        }

                    }
                }else{
                    for(RecipeItemView riv : recipeItemViewList){
                        riv.setVisible(true);
                        riv.managedProperty().bind(riv.visibleProperty());
                    }
                }
            }
        });
        // Suchfunktion
        // Ende
        
    }
    
}
