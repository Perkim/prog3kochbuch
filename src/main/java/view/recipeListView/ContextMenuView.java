package view.recipeListView;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.input.ContextMenuEvent;
import singleton.Cookbook;

/**
 * Eine Klasse fuer ein ContextMenu, das in RecipeListViewE benutzt wird. 
 * @author Vladimir Naumov
 */
public class ContextMenuView extends ContextMenu{

    /**
     * Das ContextMenuView wird fuer ein ein RecipeItemView erstellt
     * @param select is the selected RecipeItemView
     */
    public ContextMenuView(RecipeItemView select){
        
        // Erstellung des MenuItems delete.
        // Zum loeschen vom Recipe im ausgewaelten RecipeItemView.
        // Anfang
        MenuItem delete = new MenuItem("delete");
        
        delete.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Cookbook.getInstance().deleteRecipe(select.getRecipe());
            }
        });
        
        getItems().addAll(delete);
        // Ende
        
        
        
        // Das aufrufen/zeigen der ContextMenuView
        // Bei Rechtsklick auf das RecipeItemView
        // Anfang
        select.setOnContextMenuRequested(new EventHandler<ContextMenuEvent>() {
            @Override
            public void handle(ContextMenuEvent value) {
                if(isShowing()){
                    hide();
                }
                show(select, value.getScreenX(), value.getScreenY());
            }
        });
        // Ende
    }

}
