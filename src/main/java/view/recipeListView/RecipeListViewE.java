package view.recipeListView;

import entities.Recipe;
import javafx.geometry.Insets;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.VBox;
import singleton.Cookbook;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.concurrent.CompletableFuture;
import view.MainView;

/**
 * Eine Klasse zum Darstellen aller Rezepte die in Cookbook sind.
 * @author Vladimir Naumov
 */
public class RecipeListViewE extends VBox implements Observer {

    // Eine Liste aller RecipeItemViews
    private List<RecipeItemView> recipeItemViewList;
    
    // Die Mausklick ueberpruefung
    private ClickRunner latestClickRunner = null;
    
    // ContextMenu fuer die RecipeItemViews
    private ContextMenuView cmv;
    
    // Layout
    private final VBox innerBox;
    private final ScrollPane outerScrollpane;
    private final SearchView searchView;
    // Layout
    
    // Das ausgewaelte Rezept
    private RecipeItemView select;

    private final MainView mainView;

    /**
     * Erstelung der RecipeListViewE basierend an einer VBox.
     * @param main is the MainView of the stage and used to define the size
     */
    public RecipeListViewE(MainView main) {

        this.mainView = main;
        Cookbook.getInstance().addObserver(this);
        
        // Ein ScrollPane in dem die innerBox angezeigt werden.
        outerScrollpane = new ScrollPane();
        
        // Eine VBox, in der die RecipeItemViews angezeigt werden.
        innerBox = createInnerBox(Cookbook.getInstance().getRecipeArrayList());

        // outerScrollpane einstellungen
        outerScrollpane.setPadding(new Insets(0,0,0,0));
        outerScrollpane.prefWidthProperty().bind(this.widthProperty());
        outerScrollpane.prefViewportWidthProperty().bind(this.widthProperty().subtract(12));
        outerScrollpane.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        outerScrollpane.setVbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);
        // outerScrollpane einstellungen
        
        
        // Die Suchfunktion
        searchView = new SearchView(recipeItemViewList);
        searchView.prefWidthProperty().bind(this.widthProperty());

        
        getChildren().addAll(searchView, outerScrollpane);

        this.prefWidthProperty().bind(main.widthProperty().subtract(0));

    }
    
    /**
     * Erstellt die VBox, in der die RecipeItemViews angezeigt werden.
     * @param recipe is a list of Recipe that includs all recipes that nead a RecipeItemView 
     * @return the VBox with all the RecipeItemViews in it. 
     */
    private VBox createInnerBox(List<Recipe> recipe){
        recipeItemViewList = createRecipeItemViewList(recipe);

        VBox inBox = new VBox();
        inBox.getChildren().addAll(recipeItemViewList);
        
        // Setzt den Inhald der outerScrollpane mit der innerBox neu.
        outerScrollpane.setContent(inBox);
        
        inBox.prefWidthProperty().bind(outerScrollpane.prefViewportWidthProperty());
        
        RecipeItemViewMouseClick();
        
        return inBox;
    }

    
    /**
     * Erstellt eine Liste von RecipeItemView
     * @param recipe the to used recipes
     * @return a List of RecipeItemView
     */
    private List<RecipeItemView> createRecipeItemViewList(List<Recipe> recipe){
        List <RecipeItemView> rivList = new ArrayList<>();
        
        for(Recipe r : recipe) {
            RecipeItemView rView = new RecipeItemView(r);
            rivList.add(rView);
        }
        
        return rivList;
    }

    /**
     * Ueberprueft welcher Mausklick benutzt wurde und 
     * was in diesem Fall getann werden muss.
     */
    public void RecipeItemViewMouseClick(){
        for(int i = 0; i < recipeItemViewList.size(); i++){
            final RecipeItemView riv = recipeItemViewList.get(i);
            riv.setOnMouseClicked(me -> {
                // Selektiert das angeklickte RecipeItemView
                select = riv;
                
                // Verstelckt das ContextMenu falls sichtbar.
                if(cmv != null && cmv.isShowing()){
                    cmv.hide();
                }
                
                switch (me.getButton()){
                    // Linksklick
                    case PRIMARY:
                        // Singelklick
                        if(me.getClickCount() == 1){
                            latestClickRunner = new ClickRunner(() -> {
                                // SingelClick
                            });
                            CompletableFuture.runAsync(latestClickRunner);
                        }
                        // Doppelklick
                        if(me.getClickCount() == 2){
                            // Singelklick abbrechen
                            if(latestClickRunner != null){
                                latestClickRunner.abort();
                            }
                            
                            // zeige recipeView vom angeklicktem RecipeItemView
                            mainView.showRecipe(riv.getRecipe());
                        }
                        break;
                    
                    // Rechtsklick
                    case SECONDARY:
                        
                        // Zeige das ContextMenu fuer das asugeweahlte RecipeItemView
                        cmv = new ContextMenuView(getSelect());
                        break;
                        
                    // Mittelklick, etc...
                    default:
                        // Mitteklick, etc ...
                        break;
                }
            });
        }
    }
    
    /**
     * Eine Methode zur zurueckgabe des ausgewaelten RecipeItemView
     * @return the selected RecipeItemView
     */
    public RecipeItemView getSelect(){
        System.out.println(select);
        return select;
    }
    
    /**
     * Eine Methode zum updaten der benutzten RecipeList.
     * @param o
     * @param arg 
     */
    @Override
    public void update(Observable o, Object arg) {
        System.out.println("Begonnen");
        Cookbook c = (Cookbook) o;
        //recipeList = c.getRecipeArrayList();
        createInnerBox(Cookbook.getInstance().getRecipeArrayList());
        System.out.println("ende");
    }
}
