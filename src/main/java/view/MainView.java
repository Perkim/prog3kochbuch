package view;


import javafx.scene.Parent;
import view.recipeListView.RecipeListViewE;
import entities.Recipe;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import view.recipeWizard.RecipeWizardView;
import view.recipeView.RecipeView;

/**
 * MainView is our primary stage - every other view is in here.
 * We change views be replacing the current center of this view.
 * All views are comprised of subviews - for easier editing. The smaller the responsibility of parts, the more reusable they become.
 * @author VNaumov,JGolovencic,VKhablov, Dennis Weisenseel
 */

public class MainView extends BorderPane {

    private Stage primaryStage;
    private RecipeListViewE recipeListView;
    private RecipeWizardView recipeWizardView;
    private MenuBarView menuBarView;
    private Recipe recipe;

    private Scene mainScene;

    public MainView(Stage primaryStage) {
        this.primaryStage = primaryStage;

        mainScene = new Scene(this,800,600);
        menuBarView = new MenuBarView(this);

        System.out.println(primaryStage.heightProperty());
        System.out.println(primaryStage.widthProperty());
        this.prefHeightProperty().bind(primaryStage.heightProperty());
        this.prefWidthProperty().bind(primaryStage.widthProperty());
        System.out.println(this.heightProperty());
        System.out.println(this.widthProperty());
        recipeListView = new RecipeListViewE(this);

        this.setTop(menuBarView);
        menuBarView.toggleSaveAndPrint(true);
        setCenter(recipeListView);
        
    }

    public void show() {
        primaryStage.setScene(mainScene);
        primaryStage.show();
    }

    public void showRecipeList() {
        menuBarView.toggleSaveAndPrint(true);
        setCenter(recipeListView);
    }

    public void showRecipeWizard(Recipe r) {
        recipeWizardView = new RecipeWizardView(this,r);
        setCenter(recipeWizardView);
    }

    public void showRecipe(Recipe r) {
        RecipeView recipeView = new RecipeView(this, r);
        this.recipe = r;
        menuBarView.toggleSaveAndPrint(false);
        setCenter(recipeView);
    }

    public Recipe getSelectedRecipe() {
        return recipe;
    }

    public void setView(Parent root) {
        setCenter(root);
    }
}