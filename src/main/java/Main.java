import javafx.application.Application;
import javafx.stage.Stage;
import singleton.Cookbook;
import view.MainView;

import java.io.IOException;


/**
 * Entry-Point. Loads database entries, launches.
 * If we want persistence between launches toggle
 * <property name="hibernate.hbm2ddl.auto" value="create"/> to update or remove entry. (in persistence.xml)
 * @Author Dennis Weisenseel
 */

public class Main extends Application {

    public static void main( String args[] ) throws IOException {
        Cookbook k = Cookbook.getInstance();
        k.init();
        launch();
        k.close();
    }

    public void start(Stage primaryStage) throws Exception {
        MainView view = new MainView(primaryStage);
        view.show();
    }
}
