package entities;

import singleton.Cookbook;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author Dennis Weisenseel
 * Entity to represent a recipe
 * Made for Hibernate
 */
@Entity
public class Recipe implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    private String name;
    private String path;
    private String description;
    //private String category;

    @OneToMany(cascade = CascadeType.MERGE)
    private List<RecipeStep> steps = new ArrayList<RecipeStep>();

    @OneToMany(cascade = CascadeType.MERGE)
    private List<Ingredient> ingredients = new ArrayList<Ingredient>();

    public Recipe() {

    }

    public Recipe(String name, String path, String description) {
        this.name = name;
        this.path = path;
        this.description = description;
    }

    /*public Recipe(String name, String path, String description, String category) {
        this.name = name;
        this.path = path;
        this.description = description;
        this.category = category;
    }*/

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    //public String getCategory() { return category; }

    //public void setCategory(String category) { this.category = category; }

    public String getDescription() { return description; }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<RecipeStep> getSteps() {
        return steps;
    }

    public void setSteps(List<RecipeStep> steps) {
        this.steps = steps;
    }

    public void addIngredient(Ingredient i) {
        this.ingredients.add(i);
    }

    public void removeIngredient(Ingredient i) {
        this.ingredients.remove(i);
    }

    public void setIngredients(List<Ingredient> ingredients) {
        this.ingredients = ingredients;
    }

    public List<Ingredient> getIngredients() {
        return ingredients;
    }
}
