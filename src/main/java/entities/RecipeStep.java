package entities;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @Author Dennis Weisenseel
 * Entity to represent a step in recipe
 * Made for Hibernate
 */
@Entity
public class RecipeStep implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;
    private String path;
    private String description;

    public RecipeStep() {

    }

    public RecipeStep(String path, String description) {
        setPath(path);
        setDescription(description);
    }

    //neuer Konstruktor zum anpassen eines Steps oder hinzufügen eines Bildes
    public RecipeStep(String change) {
        setDescription(change);
    }



    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return new String(path + " " + description);
    }
}