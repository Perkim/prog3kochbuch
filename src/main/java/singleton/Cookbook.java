package singleton;

import entities.Ingredient;
import entities.Recipe;
import entities.RecipeStep;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Observable;

/**
 * A singleton meant to represent a real cookbook. You can add Recipes, change Recipes and remove Recipes.
 * You can also manage ingredients, if you want to keep track of prices.
 *
 * Built with Hibernate and the Java Persistence API. We hold the current data in
 * memory as well as save it to the database on change.
 * @Author Dennis Weisenseel
 */

public class Cookbook extends Observable {
    private static Cookbook ourInstance = new Cookbook();

    /**
     * Singleton-Access method.
     */

    public static Cookbook getInstance() {
        return ourInstance;
    }

    /**
     * An arrayList that holds information on current recipes.
     */
    private ArrayList<Recipe> recipeArrayList;

    /**
     * An arrayList that holds information on current Ingredients.
     */
    private ArrayList<Ingredient> ingredientArrayList;

    /**
     * EntityManager (JavaPersistence API)
     */
    private EntityManagerFactory emf;

    /**
     * EntityManager (JavaPersistence API)
     */
    private EntityManager em;

    /**
     * Empty Constructor
     */

    private Cookbook() {

    }

    /**
     * Initializes the Persistence Manager with our sample database.
     * Loads all recipes and ingredients from database into memory.
     */
    public void init() {
        emf = Persistence.createEntityManagerFactory("sample");
        em = emf.createEntityManager();

        //load all recipes

        String recipeQuery = "FROM Recipe";
        TypedQuery<Recipe> recipeTypedQuery = em.createQuery(recipeQuery, Recipe.class);
        List<Recipe> resultsRecipes = recipeTypedQuery.getResultList();

        String ingredientQuery = "FROM Ingredient";
        TypedQuery<Ingredient> ingredientTypedQuery = em.createQuery(ingredientQuery, Ingredient.class);
        List<Ingredient> resultsIngredients = ingredientTypedQuery.getResultList();


        recipeArrayList = new ArrayList<Recipe>(resultsRecipes);
        ingredientArrayList = new ArrayList<Ingredient>(resultsIngredients);

        System.out.println("Loaded " + recipeArrayList.size() + " recipes.");
        System.out.println("Loaded " + ingredientArrayList.size() + " ingredients");
    }

    /**
     * Adds a Recipe to database as well as memory.
     * @param r - A Recipe entity to add
     */

    public void addRecipe(Recipe r) {
        em.getTransaction().begin();
        em.merge(r);
        em.getTransaction().commit();
        if(!recipeArrayList.contains(r)) {
            recipeArrayList.add(r);
        }
        setChanged();
        notifyObservers();
    }

    /**
     * Deletes a Recipe to database as well as memory.
     * @param r - A Recipe entity to delete
     */
    public void deleteRecipe(Recipe r) {
        em.getTransaction().begin();
        em.remove(r);
        em.getTransaction().commit();
        recipeArrayList.remove(r);
        setChanged();
        notifyObservers();
    }


    public ArrayList<Recipe> getRecipeArrayList() {
        return recipeArrayList;
    }

    /**
     * Updates a Recipe to database as well as memory.
     * @param r - A Recipe entity to Updates
     */

    public void update(Recipe r) {
        em.getTransaction().begin();
        em.persist(r);
        em.getTransaction().commit();
        setChanged();
    }

    /**
     * helper method for testing
     * @return
     */
    public Recipe getDebugRecipe() {
        Recipe r = new Recipe();
        r.setDescription("Ein Testrezept mit zahlreichen kleinen Dingen drin. " +
                "Fütter mich. Mach mich. Koch mich. Egal was. \n DEIN DING WAS" +
                " ABGEHT \n Bitte Linebreaks unterstützen und automatischen Linewrap ty");
        r.setName("VieleSteps");
        r.setPath("/images/lachsrolle.jpg");

        Ingredient zwiebel = new Ingredient("Zwiebel",  10, "Stück" );
        Ingredient tomaten = new Ingredient("Tomate",  13, "Stück" );
        Ingredient zucker = new Ingredient("Zucker",  10, "Gramm" );
        Ingredient salz = new Ingredient("Salz", 0,  "Tonnen" );

        Ingredient q = new Ingredient("Zwiebel",  10, "Stück" );
        Ingredient t = new Ingredient("Tomate",  13, "Stück" );
        Ingredient z = new Ingredient("Zucker",  10, "Gramm" );
        Ingredient s = new Ingredient("Salz", 0,  "Tonnen" );

        ArrayList<Ingredient> ingredients = new ArrayList<>();

        ingredients.add(zwiebel);
        ingredients.add(tomaten);
        ingredients.add(zucker);
        ingredients.add(salz);
        ingredients.add(q);
        ingredients.add(t);
        ingredients.add(z);
        ingredients.add(s);



        r.setIngredients(ingredients);

        RecipeStep step1 = new RecipeStep("/images/lachsrolle.jpg", "Ein Testrezept mit zahlreichen kleinen Dingen drin. ");

        RecipeStep step2 = new RecipeStep("/images/lachsrolle.jpg", "Ein Testrezept mit zahlreichen kleinen Dingen drin. " +
                "Fütter mich. Mach mich. Koch mich. Egal was. \n DEIN DING WAS" +
                " ABGEHT \n Bitte Linebreaks unterstützen und automatischen Linewrap ty");
        RecipeStep step3 = new RecipeStep("/images/lachsrolle.jpg", "Ein Testrezept mit zahlreichen kleinen Dingen drin. " +
                "Fütter mich. Mach mich. Koch mich. Egal was. \n DEIN DING WAS");
        RecipeStep step4 = new RecipeStep("/images/lachsrolle.jpg", "Ein Testrezept mit zahlreichen kleinen Dingen drin. " +
                "Fütter mich. Mach mich. Koch mich. Egal was. \n DEIN DING WAS" +
                " ABGEHT \n Bitte Linebreaks unterstützen und automatischen Linewrap ty");
        RecipeStep step5 = new RecipeStep("/images/lachsrolle.jpg", "Ein Testrezept mit zahlreichen kleinen Dingen drin. " +
                "Fütter mich. Mach mich. Koch mich. Egal was. \n DEIN DING WAS" +
                " ABGEHT \n Bitte Linebreaks unterstützen und automatischen Linewrap ty");
        RecipeStep step6 = new RecipeStep("/images/lachsrolle.jpg", "Ein Testrezept mit zahlreichen kleinen Dingen drin. " +
                "Fütter mich. Mach mich. Koch mich. Egal was. \n DEIN DING WAS" +
                " ABGEHT \n Bitte Linebreaks unterstützen und automatischen Linewrap ty");
        RecipeStep step7 = new RecipeStep("/images/lachsrolle.jpg", "Ein Testrezept mit zahlreichen kleinen Dingen drin. " +
                "Fütter mich. Mach mich. Koch mich. Egal was. \n DEIN DING WAS" +
                " ABGEHT \n Bitte Linebreaks unterstützen und automatischen Linewrap ty");
        RecipeStep step8 = new RecipeStep("/images/lachsrolle.jpg", "Ein Testrezept mit zahlreichen kleinen Dingen drin. " +
                "Fütter mich. Mach mich. Koch mich. Egal was. \n DEIN DING WAS" +
                " ABGEHT \n Bitte Linebreaks unterstützen und automatischen Linewrap ty");


        ArrayList<RecipeStep> steps = new ArrayList<RecipeStep>();
        steps.add(step1);
        steps.add(step2);
        steps.add(step3);
        steps.add(step4);
        steps.add(step5);
        steps.add(step6);
        steps.add(step7);
        steps.add(step8);


        r.setSteps(steps);

        return r;
    }

    public void close() {
        emf.close();
    }
}
