package singleton;

import entities.Recipe;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.persistence.RollbackException;

import static org.junit.jupiter.api.Assertions.*;

class CookbookTest {

    @BeforeEach
    void setUp() {
        Cookbook.getInstance().init();
    }

    @AfterEach
    void tearDown() {
    }

    /**
     * Should test whether Cookbook is instantiated
     */
    @Test
    void getInstance() {
        assert(Cookbook.getInstance() != null);
    }

    /**
     *
     */
    @Test
    void init() {
       //Check whether Cookbook is setup

       assert(Cookbook.getInstance().getRecipeArrayList() != null);
       assert(Cookbook.getInstance().getDebugRecipe() != null);
    }

    @Test
    void addRecipe() {
        Recipe r = Cookbook.getInstance().getDebugRecipe();

        Cookbook.getInstance().addRecipe(r);
        assert(Cookbook.getInstance().getRecipeArrayList().contains(r));
    }

    @Test
    void deleteRecipe() {

        Recipe r = Cookbook.getInstance().getDebugRecipe();

        Cookbook.getInstance().addRecipe(r);
        Cookbook.getInstance().deleteRecipe(r);
        assert(!Cookbook.getInstance().getRecipeArrayList().contains(r));
    }

    @Test
    void getRecipeArrayList() {
        Recipe r = Cookbook.getInstance().getDebugRecipe();

        Cookbook.getInstance().addRecipe(r);
        assert(Cookbook.getInstance().getRecipeArrayList().contains(r));
    }

    /**
     * Expecting an exception for update
     */
    @Test
    void expectFailUpdate() {
        Recipe r = Cookbook.getInstance().getDebugRecipe();

        Cookbook.getInstance().addRecipe(r);

        r.setName("Test");

        try {
            Cookbook.getInstance().update(r);
        } catch(RollbackException e) {
            assert(false);
        }

    }


    /**
     * Check whether recipe is properly created
     */
    @Test
    void getDebugRecipe() {
        assert(Cookbook.getInstance().getDebugRecipe() != null);
    }

}